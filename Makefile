outputFile = "bin/out"

all:
	@mkdir -p bin
	g++ --std=c++14 -Wall -pedantic -Wno-long-long -O2 -Werror -lcrypto -o ${outputFile} main.cpp
	@echo "Compiled to \"${outputFile}\""

run:
	@${outputFile} ${args}

clean:
	rm -f astro_*.bmp doge_*.bmp -r bin

