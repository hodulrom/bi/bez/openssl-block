// Roman Hodulák (email: hodulrom@fit.cvut.cz)
#include <cstdlib>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <cstring>
#include <cerrno>

// Additional return codes
#define EXIT_FILE_ERR 1
#define EXIT_CTX_ERR 2
#define EXIT_RAND_ERR 3

// Commands
#define COMMAND_ENCRYPT "-e"
#define COMMAND_DECRYPT "-d"

// Sizes
#define BYTE sizeof(unsigned char)
#define UINT sizeof(unsigned int)

// Modes
#define ENCRYPT 1
#define DECRYPT 0

typedef unsigned char byte;
typedef unsigned int uint;

int fileReadErr(const char * filename) {
	fprintf(stderr, "Cannot read file \"%s\": %s\n", filename, strerror(errno));

	return EXIT_FILE_ERR;
}

int fileReadErr(const char * filename, const char * errorMsg) {
	fprintf(stderr, "Cannot read file \"%s\": %s\n", filename, errorMsg);

	return EXIT_FILE_ERR;
}

int fileWriteErr(const char * filename) {
	fprintf(stderr, "Cannot write to \"%s\": %s\n", filename, strerror(errno));

	return EXIT_FILE_ERR;
}

int commandHelp(int argc, char * argv[]) {
	printf("usage: %s <command> [options]\n\n", (argc > 0 ? argv[0] : ""));
	printf("Commands:\n");
	printf("   -e <path> <key>  Encrypts BMP image with given key.\n");
	printf("   -d <path> <key>  Decrypts BMP image with given key.\n");
	printf("\nOptions:\n");
	printf("   -m <mode>        Operation mode, either cbc or ecb. Default is ecb.\n");

	return EXIT_SUCCESS;
}

int loadFile(const char * filename, byte ** buffer, uint * bufferLength, byte ** header, uint * headerLength) {
	// Open our files
	FILE * bmp = fopen(filename, "rb");

	if (bmp == NULL) {
		return fileReadErr(filename);
	}

	// Start reading
	byte magicBytes[2]; // BM in ASCII
	byte reservedBytes[4]; // This just copies over

	// Check first two first indicating if file is BMP
	if (!fread(magicBytes, BYTE, 2, bmp)) {
		fclose(bmp);
		return fileReadErr(filename);
	}

	if (magicBytes[0] != 'B' || magicBytes[1] != 'M') {
		fclose(bmp);
		return fileReadErr(filename, "Not a BMP file.");
	}

	// Read some values from header
	if (!fread(bufferLength, UINT, 1, bmp)
		|| !fread(& reservedBytes, BYTE, 4, bmp)
		|| !fread(headerLength, UINT, 1, bmp)
	) {
		fclose(bmp);
		return fileReadErr(filename);
	}

	* header = new byte[* headerLength];

	// Read whole header to copy over
	rewind(bmp);

	if (!fread(* header, BYTE, * headerLength, bmp)) {
		fclose(bmp);
		delete[] * header;
		return fileReadErr(filename);
	}

	* buffer = new byte[* bufferLength];

	// Read pixel data
	if (!fread(* buffer, BYTE, * bufferLength, bmp) || fclose(bmp) == EOF) {
		fclose(bmp);
		delete[] * header;
		delete[] * buffer;
		return fileReadErr(filename);
	}

	return EXIT_SUCCESS;
}

int saveFile(const char * filename, byte * buffer, uint bufferLength, byte * header, uint headerLength) {
	FILE * outFile = fopen(filename, "wb");

	if (outFile == NULL) {
		return fileWriteErr(filename);
	}

	// Write and flush
	if (!fwrite(header, BYTE, headerLength, outFile)
		|| !fwrite(buffer, BYTE, bufferLength, outFile)
		|| fflush(outFile) == EOF
		|| fclose(outFile) == EOF
	) {
		return fileWriteErr(filename);
	}

	return EXIT_SUCCESS;
}

int commandCipher(int argc, char * argv[], int mode) {
	if (argc != 4 && (argc != 6 || strncmp(argv[4], "-m", 3))) {
		return commandHelp(argc, argv);
	}

	// Parse mode
	const EVP_CIPHER * cipher = EVP_des_ecb();
	char extension[9] = "_ecb.bmp";

	if (argc == 6) {
		if (!strncmp(argv[5], "cbc", 4)) {
			strncpy(extension, "_cbc.bmp", 8);
			cipher = EVP_des_cbc();
		}
		else if (strncmp(argv[5], "ecb", 3)) {
			return commandHelp(argc, argv);
		}
	}

	// Read file
	const char * inputFilename = argv[2];
	const byte * key = (const byte *) argv[3];

	uint bufferInLength, headerLength;
	byte * bufferIn, * header;

	int code = loadFile(inputFilename, & bufferIn, & bufferInLength, & header, & headerLength);

	if (code != EXIT_SUCCESS) {
		return code;
	}

	// Init cipher and context
	EVP_CIPHER_CTX * ctx = EVP_CIPHER_CTX_new();

	if (ctx == NULL) {
		delete[] header;
		delete[] bufferIn;
		fprintf(stderr, "Unable to initialize EVP context\n");
		return EXIT_CTX_ERR;
	}

	// Generate initial vector
	byte initialVector[64];

	if(RAND_bytes(initialVector, sizeof(initialVector)) != 1) {
		delete[] header;
		delete[] bufferIn;
		fprintf(stderr, "Unable to generate random bytes: %s\n", ERR_error_string(ERR_get_error(), NULL));
		return EXIT_RAND_ERR;
	}

	// Do the encryption
	uint bufferOutLength = 0;
	int len = 0;
	byte * bufferOut = new byte[bufferInLength];

	EVP_CipherInit_ex(ctx, cipher, NULL, key, initialVector, mode);
	EVP_CipherUpdate(ctx, bufferOut, & len, bufferIn, bufferInLength);
	bufferOutLength += len;

	EVP_CipherFinal_ex(ctx, bufferOut + bufferOutLength, & len);
	bufferOutLength += len;

	// Clean up
	delete[] bufferIn;
	EVP_CIPHER_CTX_cleanup(ctx);

	// Init output file
	size_t inputFilenameLength = strlen(inputFilename);
	char * outputFilename = new char[inputFilenameLength + 9];
	outputFilename[0] = '\0';

	// Check for file name extensions
	if (mode == ENCRYPT
		&& inputFilenameLength > 8
		&& !strcmp(inputFilename + inputFilenameLength - 8, "_dec.bmp")
	) {
		inputFilenameLength -= 8;
	}
	else if (mode == DECRYPT
		&& inputFilenameLength > 8
		&& !strcmp(inputFilename + inputFilenameLength - 8, extension)
	) {
		inputFilenameLength -= 8;
	}
	else if (inputFilenameLength > 4 && !strcmp(inputFilename + inputFilenameLength - 4, ".bmp")) {
		inputFilenameLength -= 4;
	}

	strncat(outputFilename, inputFilename, inputFilenameLength);
	strncat(outputFilename, (mode == ENCRYPT ? extension : "_dec.bmp"), 8);

	code = saveFile(outputFilename, bufferOut, bufferOutLength, header, headerLength);

	delete[] bufferOut;
	delete[] header;

	if (code != EXIT_SUCCESS) {
		delete[] outputFilename;
		return code;
	}

	printf("%scrypted file: \"%s\"\n", (mode == ENCRYPT ? "En" : "De"), outputFilename);

	delete[] outputFilename;

	return EXIT_SUCCESS;
}

int commandEncrypt(int argc, char * argv[]) {
	return commandCipher(argc, argv, ENCRYPT);
}

int commandDecrypt(int argc, char * argv[]) {
	return commandCipher(argc, argv, DECRYPT);
}

int main(int argc, char * argv[]) {
	if (argc >= 2) {
		if (strcmp(argv[1], COMMAND_ENCRYPT) == 0) {
			return commandEncrypt(argc, argv);
		}
		else if (strcmp(argv[1], COMMAND_DECRYPT) == 0) {
			return commandDecrypt(argc, argv);
		}
	}

	return commandHelp(argc, argv);
}
