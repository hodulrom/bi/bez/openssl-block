# OpenSSL Block Cipher
Assignment of the 3rd laboratory. This program encrypts and decrypts [BMP](https://en.wikipedia.org/wiki/BMP_file_format) images with [DES](https://cs.wikipedia.org/wiki/Data_Encryption_Standard) cipher. User can choose from [ECB](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Electronic_Codebook_(ECB)) or [CBC](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Cipher_Block_Chaining_(CBC)) operation mode.

[EDUX](https://edux.fit.cvut.cz/courses/BI-BEZ/labs/03/start)

## Usage
You can build and run the application via makefile.

**Build**
```bash
make
```
**Run**
```bash
make run
```
Prints out:
```
usage: bin/prg <command> [options]

Commands:
   -e <path> <key>  Encrypts BMP image with given key.
   -d <path> <key>  Decrypts BMP image with given key.

Options:
   -m <mode>        Operation mode, either cbc or ecb. Default is ecb.

```

**Clean up build files**
```
make clean
```

## Return codes
* `0`: Success.
* `1`: File reading / writing error.
* `2`: OpenSSL EVP context error.
* `3`: OpenSSL RAND error.
